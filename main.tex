\documentclass[12pt]{article}
\usepackage{graphicx}
\usepackage{enumitem}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{amsmath} % allows you to put text in the math environment.
\usepackage{bm}
\usepackage[margin=0.7in]{geometry}
\usepackage{tikz}
\usepackage{tikz-3dplot}
\usetikzlibrary{shapes, calc, positioning}
\tdplotsetmaincoords{70}{120}

\title{\Huge\textbf{Project 3}}
\author{James Koch\\and Da Yuan} %share with Da Yuan: dayu@student.chalmers.se
\date{\oldstylenums{2018}-\oldstylenums{12}-\oldstylenums{20}}

% --------------------Tikz
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{calc}
\usetikzlibrary{patterns}

% --------------------Graphics Path
\graphicspath{ {./imgs/} }

% --------------------Indention
\setlength{\parindent}{0cm}
\setlength{\parskip}{\baselineskip}

% --------------------Colors
\usepackage{color} %red & green & blue & yellow & cyan & magenta & black & white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red & Green & Blue
\definecolor{mylilas}{RGB}{170,55,241}

\begin{document}

\lstset{language=Matlab,%
    %basicstyle=\color{red},
    breaklines=true,%
    morekeywords={matlab2tikz},
    keywordstyle=\color{blue},%
    morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
    identifierstyle=\color{black},%
    stringstyle=\color{mylilas},
    commentstyle=\color{mygreen},%
    showstringspaces=false,%without this there will be a symbol in the places where there is a space
    numbers=left,%
    numberstyle={\tiny \color{black}},% size of the numbers
    numbersep=9pt, % this defines how far the numbers are from the text
    emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
    %emph=[2]{word1,word2}, emphstyle=[2]{style},    
}

%--------------------Title Page
\maketitle
\newpage % to create a title page

\section{Introduction}
In this report, a finite element analysis of a fixed-fixed beam is performed.
Specifically, in the first section of this report a 3D elastic analysis is performed using the commercial FE-software COMSOL.
Then in the second section the problem is simplified to a 2D elastic analysis performed by developing a script in MATLAB which utilizes the FEM package CALFEM.
The beam is \(10m\) long, \(1m\) in height, and \(2m\) deep with a coordinate system defined as in Figure \ref{fig-beamgeometry}.
Please note that the coordinate system defined is not the same coordinate system defined in COMSOL by default.
By choosing the coordinate system in this way, the load is applied in the negative \(y-\)direction and the depth of the beam is in the \(z-\)direction.

\begin{figure}[h!]
  \centering
  %\includegraphics[scale=0.8]{imgs/beamgeometry.png}
  \begin{tikzpicture}[scale=2, tdplot_main_coords, axis/.style={->,dashed},thick]
  \draw[axis] (3, 0, 0) -- (-5, 0, 0) node [right] {$z$};
  \draw[axis] (0, 0, 0) -- (0, 6, 0) node [above] {$x$};
  \draw[axis] (0, 0, 0) -- (0, 0, 2) node [above] {$y$};
  
  % coordinate of vertices
  \coordinate  (d1) at (-2,0,0){};
  \coordinate  (d2) at (2,0,0){};
  \coordinate  (d3) at (2,5,0){};
  \coordinate  (d4) at (-2,5,0){};
  \coordinate  (d5) at (-2,0,1){};
  \coordinate  (d6) at (2,0,1){};
  \coordinate  (d7) at (2,5,1){};
  \coordinate  (d8) at (-2,5,1){};
    
  % fill gray color with opacity
  \fill[gray!80,opacity=0.5] (d2) -- (d3) -- (d7) -- (d6)-- cycle; 
  \fill[gray!80,opacity=0.5] (d3) -- (d7) -- (d8) -- (d4)-- cycle;
  \fill[gray!80,opacity=0.5] (d1) -- (d2) -- (d6) -- (d5)-- cycle;
  \fill[gray!80,opacity=0.5] (d1) -- (d4) -- (d8) -- (d5)-- cycle;
  \fill[gray!80,opacity=0.5] (d1) -- (d2) -- (d3) -- (d4)-- cycle;
  \fill[gray!80,opacity=0.5] (d5) -- (d6) -- (d7) -- (d8)-- cycle;
  
  % draw frames
  \draw []       (d2)--(d3)--(d7)--(d6)--(d2);
  \draw [dashed] (d1)--(d2);
  \draw []       (d5)--(d6);
  \draw [dashed] (d1)--(d5);
  \draw []       (d8)--(d5);
  \draw []       (d4)--(d8);
  \draw [dashed] (d1)--(d4);
  \draw []       (d3)--(d4);
  \draw []       (d7)--(d8);         
  
  \end{tikzpicture}
  \caption{Depiction of the geometry and coordinate system of the 3D beam analyzed.}
  \label{fig-beamgeometry}
\end{figure}

The material properties of the beam are such that the modulus of elasticity, \(E\), is equal to  \(90GPa\) and Poisson's ratio, \(\nu\) is equal to \(0.3\).
The self-weight of the beam shall be considered as \(\vec{b} = [0,\,-\rho g]^T\), where \(\rho\) is the density in \(\frac{kg}{m^3}\) and \(g\) is the gravitational constant in \(\frac{m}{s^2}\). 
Furthermore, the top of the beam is loaded externally with \(\vec{t} = [0, \,-h_y]^T\) and \(\vec{t} = [0,\,-\frac{h_y}{L_x}x]^T\) for the first and second analyses performed, respectively. 

Additionally, since each side of the beam relies on fixed condition (i.e. no displacement and moment), only one-half of the beam needs to be analyzed due to symmetry.
In order for the symmetrical boundary condition to be sufficient to solve the finite element problem, it is necessary to specify that the displacement in the x-direction is equal to zero (i.e. for compatibility with the other half of the problem domain) and the displacement in the y-direction is free to displace but the stress in the y-direction is equal to zero (i.e. traction vector is zero in the y-direction).

\section{Task 1: Elastic analysis in 3D} \label{sec:task1}
% write up results for subtasks a, d, and e
In this task as the beam we are investigating has a symmetric situation, only one half of the beam is modeled in COMSOL by using a “roller support” at the symmetric surface (i.e. midspan) which in this case is the surface normal to x-y plane at the midspan of the beam.
The body load acts at the top surface (i.e. \(xz\)-plane) in the negative \(y\)-direction and the self-weight due to gravity acts on the whole beam’s volume acts in the same direction as that of the body load.
By using a mesh size ``extra fine'' which has a maximum and minimum element sizes of \(0.175m\) and \(0.0075m\). The result of the stresses together with deformed 3D shape is shown in Figure \ref{fig-3Ddeformation}.

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.8]{imgs/figure1_extra_fine_mash_stress_solide_1.png}
  \caption{Von Mises stress of the 3-dimensional beam analyzed using COMSOL.}
  \label{fig-3Ddeformation}
\end{figure}

A cut line (cut plane 1) parallel to the \(xy\)-plane in the top of the beam is chosen to show the distribution of displacement in the \(xy\)-plane. 
The displacement in y direction shows in Figure \ref{fig-1D displacement} 

% DA: this plot should show the y-displacement along the x-direction since it is changing in this direction. 
\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.8]{imgs/extra_fine_xy_top_displacement.PNG}
  \caption{Depiction of the y-displacement (1D) along the length of the beam in the x-direction.}
  \label{fig-1D displacement}
\end{figure}


\subsection{Convergence study}
To investigate how will the mesh's element size affect the max displacement, the point at the center of beam's top surface is been chosen. In our case is the end point of cut line 1. the max displacement vs element size h shows in Figure  \ref{fig-max displacement_element size h}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.8]{imgs/maxdisp_elementh.png}
  \caption{Effect of the element size, \(h\), on the maximum vertical deflection, \(y\).}
  \label{fig-max displacement_element size h}
\end{figure}

According to figure 4 it shows, the finer the element size the bigger the max displacement is.
Furthermore,  the max displacement converged to the value which in our case is \(3.81mm\) at the finest element size . 
To evaluate this result we compared with a Euler Bernoulli Beam.
By using Equation~\ref{eq-bernoulli}, the max displacement is \(3.54mm\) which is near enough but also it is less than the result from COMSOL.

\begin{equation} \label{eq-bernoulli}
u_{max} = \frac{qL^4}{384EI}
\end{equation}

To explain this we have to compare the assumptions of these 2 methods.
The Euler Bernoulli Beam assume the cross section is always perpendicular to the bending line which means the rotation between cross section and bending line caused by shear deformation is neglected.
But the COMSOL analyzing did take this shear deformation into account.
The result is reasonable as the Euler Bernoulli Beam's method actually calculated a stiffer beam than COMSOL did. 

\subsection{Error study}
Consider the error \(e(h)\) as a function of element size in Equation~\ref{eq-error}.
This enables the convergence rate to be found with respect to decreasing the element size.

\begin{equation} \label{eq-error}
e(h) =u_{exact}-u_h
\end{equation}

By using the displacement at finest element size as the exact solution, a log-log diagram results in Figure~\ref{fig-log-log}

\begin{figure}[h!]
  \centering
  \includegraphics[scale=0.8]{imgs/loglog-diagram.png}
  \caption{Log-log plot of the vertical displacement error as a function of the element size.}
  \label{fig-log-log}
\end{figure}

As the error varies with element size quadratically as in Equation~\ref{eq-error2}, the log-log plot in Figure~\ref{fig-log-log} is linear.

\begin{equation} \label{eq-error2}
e(h) =Ch^p
\end{equation}

The result of taking the logarthim of both sides of Equation~\ref{eq-error2} is that the constant, \(C\), equals \(0.0934\) and convergence rate, \(p\), equals \(1.93\). As the convergence rate is approximately equal to \(2\), this matches the quadratic convergence shown in Equation~\ref{eq-error2}.
This is appropriate since the strong form of the elasticity problem shows the displacement as a second order derivative which when approximated using the finite element method introduces an error of the magnitude, \(O(h^2)\).

\subsection{Stress distribution at outer surface}
The stress distribution at the outer surface of the beam is plotted in Figure~\ref{fig-stressXXZZXZ-outersurface}.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/outer_extrafine_stress_xx.png}
    \caption{\(\sigma_{xx}\)}
  \end{subfigure}
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/outer_extrafine_stress_zz.png}
    \caption{\(\sigma_{zz}\)}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/outer_extrafine_stress_xz.png}
    \caption{\(\sigma_{xz}\)}
  \end{subfigure}
  \caption{Stress distribution at the outer surface of the beam.}
  \label{fig-stressXXZZXZ-outersurface} 
\end{figure}

\subsection{Stress and strain distribution at middle surface}
\label{sec:planestress}
The stress and strain distribution at the middle surface of the beam is plotted in Figure~\ref{fig-stressNstrain-XXZZXZ-middlesurface}.
From Figure~\ref{fig-stressNstrain-XXZZXZ-middlesurface}, it can be seen that \(\sigma_{zz}\) is equal to zero on the \(xy-\)plane at half the thickness in the \(z-\)direction whereas the strain, \(\epsilon_{zz}\), is not equal to zero.
Therefore it can be inferred that to simplify this 3-dimensional problem to 2 dimensions, the ``plane stress'' condition should be used since \(\sigma_{zz}\) is equal to zero and \(\epsilon_{zz}\) is not zero.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/middle_extrafine_stress1_xx.png}
    \caption{\(\sigma_{xx}\)}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/middle_extrafine_strain_xx.png}
    \caption{\(\epsilon_{zz}\)}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/middle_extrafine_stress1_zz.png}
    \caption{\(\sigma_{xz}\)}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/middle_extrafine_strain_zz.png}
    \caption{\(\epsilon_{zz}\)}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/middle_extrafine_stress1_xz.png}
    \caption{\(\sigma_{xz}\)}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.3]{imgs/middle_extrafine_strain_xz.png}
    \caption{\(\epsilon_{xz}\)}
  \end{subfigure}
  \caption{Stress and strain distribution at the middle surface of the beam.}
  \label{fig-stressNstrain-XXZZXZ-middlesurface}
\end{figure}

\section{Task 2: Elastic analysis in 2D}
% subtask a, d, f, and h
In this section, MATLAB is used to perform a elastic analysis of a 2-dimensional beam.
The provided function \lstinline{RectangleGenMesh} function is used to generate the meshes.
Both linear triangular and bilinear rectangular elements are used to compute the displacement field of this 2D beam where Figure~\ref{fig-MATLABmeshes} show the 2-dimensional meshes.

\begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[scale=0.5]{imgs/task2part1-mesh.eps}
    \caption{}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[scale=0.5]{imgs/task2part2-mesh.eps}
    \caption{}
  \end{subfigure}
  \caption{Finite element discretization for a) linear triangular and b) bilinear rectangular meshes.}
  \label{fig-MATLABmeshes}
\end{figure}

Furthermore, the resulting deformed shapes for both linear triangular and bilinear rectangular elements is shown in Figure~\ref{fig-deformedshape}.
The results of the displacement in the \(y-\) direction converges to \(-2.64mm\) and \(-2.67mm\) at the midspan (i.e. symmetric boundary) for the triangular and rectangular meshes, respectively.
With the numerical integration using 2 integration points and linear FE-approximation being used, the numerical integration will be exact with respect to the analytical integration.
This has the effect that the FE-method employed here will overestimate the stiffness of the beam with respect to the method employed in the 3D problem solved using COMSOL and the triangular mesh creates a slightly stiffer response than the rectangular mesh (i.e. more elements are used in the triangular case).

\begin{figure}[h]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[scale=0.5]{imgs/task2part1-deformed.eps}
    \caption{}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \includegraphics[scale=0.5]{imgs/task2part2-deformed.eps}
    \caption{}
  \end{subfigure}
  \caption{Finite element deformed shape for a) linear triangular and b) bilinear rectangular meshes.}
  \label{fig-deformedshape}
\end{figure}

% FOR TRIANGULAR MESH
% | Nx | Ny | Nx*Ny | y-disp      |
% |----|----|-------|-------------|
% | 10 | 1  | 10    | -1.813945mm |
% | 10 | 5  | 50    | -2.282419mm |
% | 20 | 5  | 100   | -2.633047mm |
% | 20 | 10 | 200   | -2.645320mm |
% | 20 | 15 | 300   | -2.646125mm |

% FOR RECTANGULAR MESH
% | Nx | Ny | Nx*Ny | y-disp      |
% |----|----|-------|-------------|
% | 10 | 1  | 10    | -2.157148mm |
% | 10 | 5  | 50    | -2.293517mm |
% | 20 | 5  | 100   | -2.661548mm |
% | 20 | 10 | 200   | -2.672956mm |
% | 20 | 15 | 300   | -2.675108mm |

\subsection{Strong form derivation}
% subtask b
% remember to include what ASSUMPTIONS can be made to simplify 2D analysis
The strong form of general elasticity problems can be formulated in a similar way to that of heat flow problems which were previously shown in Project 2.
The general formulation of the strong form can be thought of as introducing a conservation principle, constitutive law, and necessary boundary conditions.
In terms of elasticity problems, it is required to solve for the displacements of a solid which undergoes deformations while the internal and boundary loading is known.
The conservation principle used to formulate the governing differential equations is that of a force equilibrium.
The conservation principle can be written mathematically in terms of stresses as in Equation \ref{eq-elasticityconservationprinciple}.

\begin{equation} \label{eq-elasticityconservationprinciple}
\tilde{\nabla} \boldsymbol{\sigma} + \mathbf{b} = 0
\end{equation}
Applying the constitutive law in Equation \ref{eq-constitutivelaw} which relates stresses to strains in solid mechanics, where \(\mathbf{D}\) is the material coefficient matrix relating to the modulus of elasticity, \(E\), and Poisson's ratio, \(\nu\), the governing differential equations are on track to be expressed in terms of the displacement. 

\begin{equation} \label{eq-constitutivelaw}
\boldsymbol{\sigma} = \mathbf{D}\boldsymbol{\epsilon}
\end{equation}
The final step is to recognize that the definition of strain, as \(\boldsymbol{\epsilon} = \frac{\Delta x}{L}\), can be utilized to re-write the the stress in the constitutive law with respect to displacement.
Therefore, the final form of the governing differential equations for 3D elasticity problems is presented in Equation 

\begin{equation} \label{eq-finalgoverningdiffequation}
\tilde{\nabla}^T \mathbf{D} \tilde{\nabla} \mathbf{u} + \mathbf{b} = 0
\end{equation}
Additionally, for a 3-dimensional elasticity problem, \(\mathbf{D}\) can be prescribed mathematically using Voigt's notation as in Equation \ref{eq-Dvoigtnotation}.

\begin{equation} \label{eq-Dvoigtnotation}
\mathbf{D} = \frac{E}{(1-\nu)(1-2\nu)} \begin{bmatrix}
																	1     & \nu & \nu & 0              & 0               &  0 \\
                                                                    \nu & 1     & \nu & 0              & 0               & 0 \\
                                                                    \nu & \nu & 1     & 0              & 0               & 0 \\
                                                                    0     & 0     & 0     & 2(1+\nu) & 0               & 0 \\
                                                                    0     & 0     & 0     & 0               & 2(1+\nu) & 0 \\
                                                                    0     & 0     & 0     & 0               & 0               & 2(1+\nu)
																	\end{bmatrix}
\end{equation}
The strong form of 3D elasticity problems can then be expressed in terms of the governing differential equations and the necessary boundary conditions shown in Equation \ref{eq-strongform}. 
Here it should be noted that the boundary conditions are expressed for a simplified 2D elasticity problem assuming the stress condition of plane stress which is explained in section\ref{sec:planestress}.
The boundary conditions are shown as a sketch in Figure~\ref{fig-bcsketch}.
The one end of the beam is a fixed support while since the problem is symmetric the midspan of the beam can be considered as a ``roller support'' preventing displacement in the x-direction (i.e. along the beam horizontally).

\begin{figure}[h]
  \centering
  \includegraphics{imgs/supportbeamsketch.png}
  \caption{Sketch of the boundary conditions for the 2D elasticity problem.}
  \label{fig-bcsketch}
\end{figure}

As such the boundary conditions can be expressed as either the displacement, \(\mathbf{u}\), (i.e. Essential BC) or traction vector, \(\mathbf{t}\), (i.e. Natural BC) with respect to either the \(x-\) or the \(y-\)direction only wheras the 3rd dimension, \(z\) is neglected.

\begin{equation} \label{eq-strongform}
  \centering
  \begin{cases}
    \tilde{\nabla}^T \mathbf{D} \tilde{\nabla} \mathbf{u} + \mathbf{b} = 0 \text{, where} \\
    u_x = 0 \text{ over the left side boundary, } L_L \\
    u_y = 0 \text{ over the left side boundary, } L_L \\
    u_x = 0 \text{ over the right side boundary, } L_R \\
    t_y = 0 \text{ over the right side boundary, } L_R \\
    t_x = 0 \text{ over the top side boundary, } L_T \\
    t_y = - \frac{h_y}{L_x} x \text{ over the top side boundary, } L_T \text{, where } h_y = 1MPa \text{ and }  L_x = 5m\\
    t_x = 0 \text{ over the bottom side boundary, } L_B \\
    t_y = 0 \text{ over the bottom side boundary, } L_B \\
  \end{cases}
\end{equation}

Furthermore in the simplified 2D plane stress condition, the material coefficient matrix, \(\mathbf{D}\), can be simplified to the form shown in Equation .

\begin{equation} \label{eq-Dmatrixsimplified2D}
  \mathbf{D} = \frac{E}{(1-\nu^2)} \begin{bmatrix}
                                    1     & \nu & 0 \\
                                    \nu & 1     & 0 \\
                                    0     & 0     & \frac{1}{2}(1-\nu)
                                    \end{bmatrix}
\end{equation}

\subsection{Weak and finite element form}
% subtask c
The weak and finite element form is stated here in Equation~\ref{eq-weakform} and Equation~\ref{eq-FEform}.

\begin{equation}
  \label{eq-weakform}
  \centering
  \begin{cases}
    \int_A (\tilde{\nabla} v)^T \, t \, \mathbf{D} \, \tilde{\nabla} u \, dA = - \oint_{L_{T}} v \, \vec{t} \, t \, dL + \int_A v \, \vec{b} \, t \, dA \\
    u_x = 0 \text{ over the left side boundary, } L_L \\
    u_y = 0 \text{ over the left side boundary, } L_L \\
    u_x = 0 \text{ over the right side boundary, } L_R
  \end{cases}
\end{equation}

The FE-form is expressed in Equation~\ref{eq-FEform}.

\begin{equation}
  \label{eq-FEform}
  \centering
  \begin{cases}
    \int_A \mathbf{B}^T \, t \, \mathbf{D} \, \mathbf{B} \, dA \mathbf{u} = - \oint_{L_{T}} \mathbf{N}^T \, \vec{t} \, t \, dL + \int_A \mathbf{B} \, \vec{b} \, t \, dA \\
    u_x = 0 \text{ over the left side boundary, } L_L \\
    u_y = 0 \text{ over the left side boundary, } L_L \\
    u_x = 0 \text{ over the right side boundary, } L_R
  \end{cases}
\end{equation}

It should be noted that the boundary line integral needs only to be evaluated over the line where the linearly-varying external load is applied since elsewhere the traction vector will be zero.

\subsection{Derivation of nodal load contribution expression}
% subtask e
The derivation of the nodal load contribution from the linearly-varying traction vector, \(\vec{t} = [0;\, - \frac{h_y}{L_x}x]\) can be derived as follows from the general expression in the FE-form from Equation~\ref{eq-FEform}.
The line integral for the boundary load can be expanded as per Equation~\ref{eq-expandedFEform}.
Since 2-dimensional elements are used (i.e. either triangular or rectangular elements), the line integral is only evaluated over one edge of the element.

\begin{equation}
  \label{eq-expandedFEform}
  \oint_L
  \begin{bmatrix}
    N_1^e & 0 \\
    0 & N_1^e; \\
    N_2^e & 0; \\
    0 & N_2^e
  \end{bmatrix}
  \begin{bmatrix}
    0 \\ - \frac{h_y}{L_x}x
  \end{bmatrix}
  dL
\end{equation}

Therefore considering only \(N_1^e (- \frac{h_y}{L_x} x)\) (i.e. \(N_1^e = \frac{1}{2} (1 - s)\)), the following deriation is presented in Equation~\ref{eq-deriveFB}.
The derivation is supported by the coordinate transformation from \(x\) to \(s\) where \(x = \frac{L_e}{2} s + \frac{1}{2} (x_i + x_j)\) and \(dx = \frac{L_e}{2} ds\).

\begin{equation}
  \label{eq-deriveFB}
  \begin{aligned}
  (\frac{-h_y L_e t}{4 L_x}) \int_{-1}^{1} (1 - s)(\frac{L_e}{2} s + \frac{1}{2} (x_i + x_j))ds \\
  (\frac{-h_y L_e t}{4 L_x}) \int_{-1}^{1} ( \frac{L_e}{2} s + \frac{1}{2} (x_i + x_j) - \frac{L_e}{2} s^2 - \frac{1}{2} (x_i + x_j)s ds \\
  (\frac{-h_y L_e t}{8 L_x}) \int_{-1}^{1} ( \frac{L_e}{2} s^2 + (x_i + x_j) s - \frac{L_e}{3} s^3 - \frac{1}{2} (x_i + x_j) s^2 ds \\
  (\frac{-h_y L_e t}{8 L_x}) \left[ 2(x_i + x_j) - 2 \frac{L_e}{3} \right] \\
  \text{Therefore, } \\
  f_b^e =
  \begin{bmatrix}
    0 \\
    (\frac{-h_y L_e t}{8 L_x}) \left[ 2(x_i + x_j) - 2 \frac{L_e}{3} \right] \\
    0 \\
    (\frac{-h_y L_e t}{8 L_x}) \left[ 2(x_i + x_j) - 2 \frac{L_e}{3} \right]
  \end{bmatrix}
  \end{aligned}
\end{equation}

And this is implemented in the code shown below.

\begin{lstlisting}
%% compute the boundary load vector
% initialize matrices
fb = zeros(ndof, 1);

% define inputs
top_nodes = size(TopSide_nodes, 1);

% for loop
for i = 1:top_nodes-1
    coordElm = [Coord(TopSide_nodes(i), :);
                Coord(TopSide_nodes(i+1), :)];
    dx = coordElm(1, 1) - coordElm(2, 1);
    dy = coordElm(1, 2) - coordElm(2, 2);
    Le = sqrt(dx^2 + dy^2);    
    
    fbe = [0;
          ((Le/(2*Lx))*(mean(coordElm(:, 1)) - Le/6))*hy*t;
          0;
          ((Le/(2*Lx))*(mean(coordElm(:, 1)) + Le/6))*hy*t];
    
    dof = [2*TopSide_nodes(i) - 1, 2*TopSide_nodes(i), ...
           2*TopSide_nodes(i+1) - 1, 2*TopSide_nodes(i+1)];

    fb(dof) = fb(dof) + fbe;
end
\end{lstlisting}

\subsection{Derive equations for an isoparametric bilinear element}
% subtask g
To derive the equations for an isoparametric bilinear element, the element stiffness matrix, \(\mathbf{K^e}\), and the element load vector, \(\mathbf{f}_l\), are calculated in terms of the following integrals in Equaton~\ref{eq-KeAndfle}.

\begin{equation}
  \label{eq-KeAndfle}
  \begin{aligned}
    K^e = \int_A \mathbf{B^e}^T \, \mathbf{D} \, \mathbf{B^e} \, t \, dA \\
    f_l^e = \int_A \mathbf{N^e}^T \, \mathbf{b} \, t \, dA
  \end{aligned}
\end{equation}

Determining the 4 shape functions needed (i.e. 4-node rectangular elements) in terms of \(\psi\) and \(\eta\) are shown in Equation~\ref{eq-psieta} and the implementation is also shown below.

\begin{equation}
  \label{eq-psieta}
  \begin{aligned}
    N_1^e = \frac{1}{4} (\psi - 1) (\eta - 1) \\
    N_2^e = -\frac{1}{4} (\psi + 1) (\eta - 1) \\
    N_3^e = \frac{1}{4} (\psi + 1) (\eta + 1) \\
    N_4^e = -\frac{1}{4} (\psi - 1) (\eta + 1)
  \end{aligned}
\end{equation}

\begin{lstlisting}
% Compute the element shape functions Ne (use xsi and eta from above)
N(:, 1) =  (xsi - 1).*(eta - 1)/4;
N(:, 2) = -(xsi + 1).*(eta - 1)/4;
N(:, 3) =  (xsi + 1).*(eta + 1)/4;
N(:, 4) = -(xsi - 1).*(eta + 1)/4;
\end{lstlisting}

Therefore, the derivatives of the shape functions can also be determined in terms of \(\psi\) and \(\eta\) which are shown in Equation~\ref{eq-dpsideta}.

\begin{equation}
  \label{eq-dpsideta}
  \mathbf{dN^e}(\psi, \eta) =
  \begin{bmatrix}
    \frac{\partial N_1^e}{d\psi} = \frac{1}{4} (\eta - 1) &
    \frac{\partial N_2^e}{d\psi} = -\frac{1}{4} (\eta - 1) &
    \frac{\partial N_3^e}{d\psi} = \frac{1}{4} (\eta + 1) &
    \frac{\partial N_4^e}{d\psi} = -\frac{1}{4} (\eta + 1) &\\
    \frac{\partial N_1^e}{d\eta} = \frac{1}{4} (\psi - 1) &
    \frac{\partial N_2^e}{d\eta} = -\frac{1}{4} (\psi + 1) &
    \frac{\partial N_3^e}{d\eta} = \frac{1}{4} (\psi + 1) &
    \frac{\partial N_4^e}{d\eta} = -\frac{1}{4} (\psi - 1) &
  \end{bmatrix}
\end{equation}

Equation~\ref{eq-dpsideta} is implemented in the MATLAB code as shown below.

\begin{lstlisting}
dN = 0.25*[(eta - 1), -(eta - 1), (eta + 1), -(eta + 1);
           (xsi - 1), -(xsi + 1), (xsi + 1), -(xsi - 1)];
\end{lstlisting}

From Equation~\ref{eq-dpsideta} and the \((x, y)\) coordinates of the 4 nodes of each rectangular element, the Jacobian matrix can be established which allows for the isoparametric (i.e. unique mapping) from global to parent domains and vice versa.
Equation~\ref{eq-jacobian} shows the formula to compute the Jacobian matrix.

\begin{equation}
  \label{eq-jacobian}
  \mathbf{J} = dN^e(\psi, \eta)
  \begin{bmatrix}
    \vec{x} & \vec{y}
  \end{bmatrix}
\end{equation}

In Equation~\ref{eq-jacobian}, the vectors \(\vec{x}\) and \(\vec{y}\) contain the \(x-\) and \(y-\) coordinates of the 4 nodes in the global domain.
The derivatives of the shape functions in terms of \(x\) and \(y\) are then computed by solving Equation~\ref{eq-dNx}.

\begin{equation}
  \label{eq-dNx}
  \mathbf{dN^e}(x,y) = \mathbf{J}^{-1} \mathbf{dN^e}(\psi, \eta)
\end{equation}

This can be implemented as the following line in the MATLAB code.
It should be noted that it is better to implement the inverse in this fashion since it is more efficient than explicitly calling the MATLAB \lstinline{inv()} function (shown in the commented out line of code).

\begin{lstlisting}
dNx = JT\dN;
%dNx = inv(JT)*dN
\end{lstlisting}

From Equation~\ref{eq-dNx}, the matrix, \(\mathbf{B^e}\), can be determined by using the resulting matrix from Equation~\ref{eq-dNx}.

\begin{equation}
  \label{eq-Be}
  \mathbf{B^e} =
  \begin{bmatrix}
    \frac{\partial N_1^e}{\partial x} & 0 & \frac{\partial N_2^e}{\partial x} & 0 & \frac{\partial N_3^e}{\partial x} & 0 & \frac{\partial N_4^e}{\partial x} & 0 \\
    0 & \frac{\partial N_1^e}{\partial y} & 0 & \frac{\partial N_2^e}{\partial y} & 0 & \frac{\partial N_3^e}{\partial y} & 0 & \frac{\partial N_4^e}{\partial y} \\
    \frac{\partial N_1^e}{\partial y} & \frac{\partial N_1^e}{\partial x} & \frac{\partial N_2^e}{\partial y} & \frac{\partial N_2^e}{\partial x} & \frac{\partial N_3^e}{\partial y} & \frac{\partial N_3^e}{\partial x} & \frac{\partial N_4^e}{\partial y} & \frac{\partial N_4^e}{\partial x}
  \end{bmatrix}
\end{equation}

The \(\mathbf{B^e}\) is implemented in the code as shown below.

\begin{lstlisting}
Be(1, 1:2:8) = dNx(1, :);
Be(2, 2:2:8) = dNx(2, :);
Be(3, 2:2:8) = dNx(1, :);
Be(3, 1:2:8) = dNx(2, :);
\end{lstlisting}

Finally, to calculate the contribution to the element stiffness matrix and element load vector for each gauss integration point can be written as in Equation~\ref{eq-finalKeAndFle}.

\begin{equation}
  \label{eq-finalKeAndFle}
  \begin{aligned}
    \mathbf{K^e} &= \sum_{j=1}^{n} \sum_{i=1}^{n} \mathbf{B^e}^T(\psi, \eta) \, \mathbf{J}^{-1} \, \mathbf{D} \, (\mathbf{J}^T)^{-1} \, \mathbf{B^e}(\psi, \eta) \, t \, det(\mathbf{J}) \, H_i \, H_j \\
    \mathbf{f_l^e} &= \sum_{j=1}^{n} \sum_{i=1}^{n} \mathbf{N^e}^T \, \mathbf{b} \, t \, det(\mathbf{J}) \, H_i \, H_j
  \end{aligned}
\end{equation}

\section{Appendix}
MATLAB m-files are attached in the submission.

\end{document}