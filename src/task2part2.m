% Project 3: Task 2 (2D Steady-state Heat Transfer) - Bilinear
%            Quadrilateral Elements
% Author: James Koch and Da Yuan
%--------------------------------------------------------------------------
% PURPOSE
%   
%--------------------------------------------------------------------------
% Copyright (C) 2018 James Koch
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License v2 as published by
% the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%--------------------------------------------------------------------------

close all; clc;

%% define general parameters and properties
% geometry
Lx = 5;         % length in x-direction, m
Ly = 1;         % length in y-direction, m
t = 2;          % thickness, z-direction, m

% material
E = 90e9;       % Modulus of elasitcity, Pa
nu = 0.3;       % Poisson's Ratio
rho = 2000;     % Density, kg/m^3

% applied load
hy = -1e6;      % applied internal load, Pa
g = 9.81;       % gravitational constant, m/s^2
b = [0, -rho*g]';

% elements along each direction
Nx = 20;
Ny = 5;

%% generate the finite element mesh
elemtype = 2; % rectangles

% generate mesh
[Edof ,Dof, Coord, Ex, Ey, LeftSide_nodes, TopSide_nodes, ...
            RightSide_nodes, BottomSide_nodes, TopRighty_node, h ] = ...
            RectangleMeshGen( Lx, Ly, Nx, Ny, elemtype );

figure(1);
eldraw2(Ex, Ey);
xlabel('x [m]');
ylabel('y [m]');
savefig(1, './imgs/task2part2-mesh.fig');
saveas(1, './imgs/task2part2-mesh', 'epsc');

% define the ndof
ndof = Dof(end, end);
nelm = size(Edof, 1);

%% compute the element stiffness matrix
% initialize matrices
K = zeros(ndof);
fl = zeros(ndof, 1);

% define inputs
ptype = 1;  % plane stress
t = 2;      % thickness, m
ngp = 2;    % number of Gauss points

ep = [ptype t ngp E nu];
% ep = [ptype t ngp];
D = (E/(1 - nu^2))*[ 1, nu,              0;
                        nu,  1,          0;
                         0,  0, (1 - nu)/2];
eq = b;

% for loop
for i = 1:nelm
    % computation of Ke
%     [Ke, fe] = plani4e(Ex(i, :), Ey(i, :), ep, D, eq);
    [Ke, fe] = plan4bilin(Ex(i, :), Ey(i, :), ep, eq);
    % assembly of Ke into K
    [K, fl] = assem(Edof(i, :), K, Ke, fl, fe);
end

%% compute the boundary load vector
% initialize matrices
fb = zeros(ndof, 1);

% define inputs
top_nodes = size(TopSide_nodes, 1);

% for loop
for i = 1:top_nodes-1
    coordElm = [Coord(TopSide_nodes(i), :);
                Coord(TopSide_nodes(i+1), :)];
    dx = coordElm(1, 1) - coordElm(2, 1);
    dy = coordElm(1, 2) - coordElm(2, 2);
    Le = sqrt(dx^2 + dy^2);    
    
    fbe = [0;
          ((Le/(2*Lx))*(mean(coordElm(:, 1)) - Le/6))*hy*t;
          0;
          ((Le/(2*Lx))*(mean(coordElm(:, 1)) + Le/6))*hy*t];
    
    dof = [2*TopSide_nodes(i) - 1, 2*TopSide_nodes(i), ...
           2*TopSide_nodes(i+1) - 1, 2*TopSide_nodes(i+1)];

    fb(dof) = fb(dof) + fbe;
end

%% define the Essential BCs
% left side boundary
xdofLeft = 2*LeftSide_nodes - 1;
ydofLeft = 2*LeftSide_nodes;

% right side boundary
xdofRight = 2*RightSide_nodes - 1;

% define bc
bc = [xdofLeft;
      ydofLeft;
      xdofRight];
bc(:, 2) = zeros(size(bc(:, 1)));

%% solve the FE-system
u = solveq(K, fl+fb, bc);

%% Plotting the deformed shape
ed = extract(Edof, u);

figure(2);
plotpar=[1, 1, 1];
sfac=250;
eldisp2(Ex, Ey, ed, plotpar, sfac);
xlabel('x [m]');
ylabel('y [m]');
savefig(2, './imgs/task2part2-deformed.fig');
saveas(2, './imgs/task2part2-deformed', 'epsc');

%% Reporting maximum displacement @ midspan
fprintf('| %d | %d | %d | %.6fmm\n', Nx, Ny, Nx*Ny, u(40)*1000);

