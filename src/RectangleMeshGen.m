function [Edof ,Dof, Coord, Ex, Ey, LeftSide_nodes, TopSide_nodes, ...
          RightSide_nodes, BottomSide_nodes, TopRighty_node, h ] = ...
          RectangleMeshGen( Lx, Ly, Nx, Ny, elemtype )

% [ Edof, Dof, Coord, Ex, Ey, LeftSide_nodes, TopSide_nodes, ...
%   RightSide_nodes, BottomSide_nodes, TopLeftElem_no, TopRighty_node, ...
%   h ] = RectangleMeshGen( Lx, Ly, Nx, Ny, elemtype )         
%-------------------------------------------------------------------------         
% Purpose: Generates a structured mesh of constant strain triangles or 
%          rectangular bilinear elements
%-------------------------------------------------------------------------
% Input: Lx       is the length in x-direction
%        Ly       is the length in y-direction
%        Nx       is the numer of element side lengths in the x-direction
%        Ny       is the numer of element side lengths in the y-direction
%        elemtype Set to 1 for CST elements(triangles) and set as 2 for 
%                 4-node bilinear elements
%-------------------------------------------------------------------------
% Output: Edof  is the element topology matrix (for 2 dof per node)
%         Dof   is the global nodal dofs matrix 
%         Coord is the global nodal coordinate matrix
%         Ex    is the element nodal coordinates in x-direction
%         Ey    is the element nodal coordinates in y-direction
%
% LeftSide_nodes     is a list of the nodes on the left boundary Gamma_4
% TopSide_nodes      is a list of the nodes on the top boundary Gamma_1
% RightSide_nodes    is a list of the nodes on the right boundary Gamma_2
% BottomSide_nodes   is a list of the nodes on the bottom boundary Gamma_3
%
% TopRighty_node     is the node at the top right corner
%
% h                  is the characteristic element size
%-------------------------------------------------------------------------
% Developed for VSM167 FEM BASICS 2011 by Håkan Johansson
% Dept. Applied Mechanics, Chalmers
%-------------------------------------------------------------------------

x_div=linspace(0,Lx,Nx+1);  % division in "squares" in x-direction

y_div=linspace(0,Ly,Ny+1);

[X_squares,Y_squares]=meshgrid(x_div,fliplr(y_div));

if elemtype==1      %triangles
    
    % introduce mid-nodes in each "square" element
    X_mids=(X_squares(1:end-1,1:end-1)+X_squares(1:end-1,2:end)+X_squares(2:end,1:end-1)+X_squares(2:end,2:end))/4;
    Y_mids=(Y_squares(1:end-1,1:end-1)+Y_squares(2:end,1:end-1))/2;
    
    X_coord=X_squares(1,:)';
    Y_coord=Y_squares(1,:)';
    for k=1:length(y_div)-1   % each row of "squares"
        X_coord=[X_coord;X_mids(k,:)';X_squares(k+1,:)'];
        Y_coord=[Y_coord;Y_mids(k,:)';Y_squares(k+1,:)'];
    end
    
    
    Row_nod_top=[1:Nx;               % Each square split in 4 elements (layout "X")
        Nx+2:2*Nx+1;         % the top elements in row
        2:Nx+1]';
    
    Row_nod_left=[1:Nx;              % Left elements
        2*Nx+2:3*Nx+1;
        Nx+2:2*Nx+1]';
    
    Row_nod_right=[2:Nx+1;           % Right elements
        Nx+2:2*Nx+1;
        2*Nx+3:3*Nx+2]';
    
    Row_nod_bottom=[Nx+2:2*Nx+1;      % Bottom elements
        2*Nx+2:3*Nx+1;
        2*Nx+3:3*Nx+2]';
    
    Row_nod=[Row_nod_top;Row_nod_left;Row_nod_right;Row_nod_bottom]; % all elements in one row of "squares"
    
    Enod=[];
    for k=1:length(y_div)-1   % each row of "squares"
        Enod=[Enod;Row_nod+(k-1)*(2*Nx+1)]; % next row has the same element/nodes, but are shifted 2*n+1 (=number of nodes on one row)
    end
    Enod=[(1:size(Enod,1))',Enod];   % add element numbers
    
    Edof=[Enod(:,1), 2*Enod(:,2)-1,2*Enod(:,2),2*Enod(:,3)-1,2*Enod(:,3),2*Enod(:,4)-1,2*Enod(:,4)];
    Ex=X_coord(Enod(:,2:end));
    Ey=Y_coord(Enod(:,2:end));
    
    % Characteristic element size is the mean of the longest distance between
    % two nodes in the element
    ElemDist=[sqrt((Ex(:,1)-Ex(:,2)).^2+(Ey(:,1)-Ey(:,2)).^2), sqrt((Ex(:,2)-Ex(:,3)).^2+(Ey(:,2)-Ey(:,3)).^2), sqrt((Ex(:,1)-Ex(:,3)).^2+(Ey(:,1)-Ey(:,3)).^2)];
    h=mean(max(ElemDist,[],2));
    
    TopRighty_node=Nx+1;
    
elseif elemtype==2      % bilinear quadrilaterals
    
    X_coord=reshape(X_squares',[],1);
    Y_coord=reshape(Y_squares',[],1);
    
    Row_nod=[1:Nx;
        Nx+2:2*Nx+1;
        Nx+3:2*Nx+2;
        2:Nx+1]';
    
    Enod=[];
    for k=1:length(y_div)-1   % each row of "squares"
        Enod=[Enod;Row_nod+(k-1)*(Nx+1)]; % next row has the same element/nodes, but are shifted 2*n+1 (=number of nodes on one row)
    end
    Enod=[(1:size(Enod,1))',Enod];   % add element numbers
    
    Edof=[Enod(:,1), 2*Enod(:,2)-1,2*Enod(:,2),2*Enod(:,3)-1,2*Enod(:,3),2*Enod(:,4)-1,2*Enod(:,4),2*Enod(:,5)-1,2*Enod(:,5)];
    
    
    TopRighty_node=Nx+1;
    
    % Characteristic element size is the mean of the longest elements side
    % assume bilinear elements are rectangular
    Ex=X_coord(Enod(:,2:end));
    Ey=Y_coord(Enod(:,2:end));
    
    ElemDist=[sqrt((Ex(:,1)-Ex(:,2)).^2+(Ey(:,1)-Ey(:,2)).^2), sqrt((Ex(:,2)-Ex(:,3)).^2+(Ey(:,2)-Ey(:,3)).^2)];
    h=mean(max(ElemDist,[],2));
    
    
end

Coord=[X_coord, Y_coord];

nnodes = size( Coord, 1 );

Dof = zeros( nnodes, 2 );

for nodeIndex = 1:nnodes
 
 Dof( nodeIndex, 1 ) = nodeIndex*2 - 1;
 Dof( nodeIndex, 2 ) = nodeIndex*2;
    
end

LeftSide_nodes=find(abs(X_coord-0)<100*eps);
RightSide_nodes=find(abs(X_coord-Lx)<100*eps);
BottomSide_nodes=find(abs(Y_coord-0)<100*eps);
TopSide_nodes=find(abs(Y_coord-Ly)<100*eps);

end

