function [ rate, constant ] = ConvergenceRate( Meas, h )
% Estimates the convergence exponent p in the form e=Ch^p using
% least-squares
% call:
% [rate, constant]=ConvergenceRate(Meas,h)
% Input -------------------------------------------------------------------
%
% Meas - vector containing values of the measure. the last element is taken
% as "exact".
%
% h - corresponding vector containing values of element size h
%
% Output ------------------------------------------------------------------
%
% rate - the estimated exponent p
%
% constant - the estimated constant C
%
%--------------------------------------------------------------------------
% Developed for VSM167 FEM BASICS 2011 by Håkan Johansson
% Dept. Applied Mechanics, Chalmers
%-------------------------------------------------------------

e = abs( Meas( 1:end - 1 ) - Meas(end) );

h = reshape( h( 1:end - 1 ), [], 1 );
e = reshape( e, [], 1 );

Vec = [ log(h) ones( length(h), 1 ) ]\log(e);

rate = Vec(1);
constant = exp( Vec(2) );

end