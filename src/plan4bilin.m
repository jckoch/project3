  function [Ke, fe] = plan4bilin(ex, ey, ep, eq)
% [ Ke, fe] = plan4bilin( ex, ey, ep, eq )
%-------------------------------------------------------------
% PURPOSE
%  Compute the stiffness matrix and element external force vector
%  for a bilinear plane stress or plane strain element including 
%  influence of out-of-plane stress (or strain)
%
% INPUT:  ex = [ x1 x2 x3 x4 ]         element nodal x-coordinates
%         ey = [ y1 y2 y3 y4 ]         element nodal y-coordinates
%
%         ep = [ ptype t ngp Emod ny ] ptype = 1 for plane stress = 1, or
%                                      ptype = 2 for plane strain
%                                        t: thickness
%                                      ngp: number of Gauss points in each
%                                           direction ( ksi and eta)
%                                           (ngp = 1 or 2 or 3)
%                                     Emod: Modulus of elasticity
%                                       ny: Poisson's ratio
%
%         eq = [ bx;                bx: body force x-dir
%                by ]               by: body force y-dir
%--------------------------------------------------------------
% OUTPUT: Ke : element stiffness matrix (8 x 8)
%         fe : equivalent nodal forces (8 x 1)
%--------------------------------------------------------------
% Developed by Peter Möller Dept. Applied Mechanics, Chalmers
%
% MODIFIED for VSM167 FEM BASICS by Dimosthenis Floros 20151201
%--------------------------------------------------------------

% Pick out parameters from ep to facilitate readability of the code

ptype = ep(1);
t     = ep(2);
ngp   = ep(3)^2; % Total gauss points = ( NoGaussPoits per direction )^2
E     = ep(4);
nu    = ep(5);

if nargin == 3
    b = zeros(2, 1);
else
    b = eq;
end

%  Initialize Ke and fe with zeros for all of their elements

Ke = zeros(8);
fe = zeros(8, 1);

%  Tolerance for the Jacobian determinant

minDetJ = 1.e-16;

% Determine constitutive matrix D for plane strain or plane stress

if ptype == 1
    D = (E/(1 - nu^2))*[ 1, nu,            0;
                        nu,  1,            0;
                         0,  0, (1 - nu)/2];
else
    D = (E/((1 - nu^2)*(1 - 2*nu)))*[nu, 1 - nu, 0;
                                     1 - nu, nu, 0;
                                     0, 0, (1 - nu)/2];
end

%  Set the appropriate Gauss integration scheme for 1, 2 or 3 Gauss points
%  in each direction (ksi, eta). (or else 1, 4 or 9 Gauss points in total)

if ngp == 1
    intWeight = [2; 2];
    GaussPoints = [0; 0];
elseif ngp == 4
    intWeight = [1; 1; 1; 1];
    GaussPoints = (1/sqrt(3))*[-1; 1; -1; 1];
elseif ngp == 9
    intWeight = (1/9)*[5; 8; 5; 8; 5; 8; 5; 8; 5];
    GaussPoints = (sqrt(3/5))*[-1; -0; 1; -0; 1; 0; -1; 0; 1];
else
    error('Only 1,2 or 3 Gauss Points in each direction apply')
end

%  Loop over all integration points to compute Ke and fe 

for gpIndex = 1:ngp
 
    xsi       = GaussPoints(gpIndex); 
    weightXsi = intWeight(gpIndex);
    eta       = GaussPoints(gpIndex);
    weightEta = intWeight(gpIndex);
 
    % Compute the element shape functions Ne (use xsi and eta from above)

    N(:, 1) =  (xsi - 1).*(eta - 1)/4;
    N(:, 2) = -(xsi + 1).*(eta - 1)/4;
    N(:, 3) =  (xsi + 1).*(eta + 1)/4;
    N(:, 4) = -(xsi - 1).*(eta + 1)/4;

    % Compute derivatives (with respect to xsi and eta) of the
    % shape functions at coordinate (xsi,eta). Since the element is
    % isoparametic, these are also the derivatives of the basis functions.

        dN = 0.25*[(eta - 1), -(eta - 1), (eta + 1), -(eta + 1);
                   (xsi - 1), -(xsi + 1), (xsi + 1), -(xsi - 1)];

    %  Use shape function derivatives and element vertex coordinates 
    %  to establish the Jacobian matrix.

    JT = dN*[ex; ey]';

    %  Compute the determinant of the Jacobian and check that it is OK

    detJ = det(JT);
    
    if ( detJ < minDetJ )
        fprintf( 1, 'Bad element geometry in function plan4bilin: detJ = %0.5g\n', detJ );
        break;   
    end

    % Determinant seems OK - invert the transpose of the Jacobian
    
    % Not need since matrix division, A\b, is faster and more accurate than
    % inv(A)*b multiplication.
    
    % Compute derivatives with respect to x and y, of all basis functions,

    dNx = JT\dN;
    
    % Use the derivatives of the shape functions to compute the element
    % B-matrix, Be

    Be(1, 1:2:8) = dNx(1, :);
    Be(2, 2:2:8) = dNx(2, :);
    Be(3, 2:2:8) = dNx(1, :);
    Be(3, 1:2:8) = dNx(2, :);
    
    % Use the shape functions to compute the element N-matrix, Ne
    
    Ne(1, 1:2:8) = N;
    Ne(2, 2:2:8) = N;

    % Compute the contribution to element stiffness and load matrix 
    % from current Gauss point
    % (check for plane strain or plane stress again!)     

    Ke = Ke + Be'*D*t*Be*detJ*weightXsi*weightEta;
    fe = fe + Ne'*b*t*detJ*weightXsi*weightEta;
    
end

end

%--------------------------end--------------------------------

